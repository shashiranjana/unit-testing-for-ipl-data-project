Explain: This data assignment you will transform raw data from IPL into unit test cases.

Dataset: matches.csv and deliveries.csv
Generate the following plots ...

Generate test cases of the number of matches played per year of all the years in IPL.
Generate test cases matches won of all teams over all the years of IPL.
For the year 2008,2010 generate test cases the extra runs conceded per team.
For the year 2008,2010 generate test cases for the top economical bowlers.


Specificaion:
Python 3.7
IDE- VS Code


