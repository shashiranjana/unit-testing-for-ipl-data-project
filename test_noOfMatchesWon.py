from noOfMatchesWon import readAndStoreData,calculate_no_of_win
import unittest
class test_noOfMatchesWon(unittest.TestCase):
    def test(self):
        actual_output ={'2017': [('Chennai Super Kings', 0), ('Kolkata Knight Riders', 1), ('Rising Pune Supergiant', 1), ('Royal Challengers Bangalore', 0)], '2008': [('Chennai Super Kings', 1), ('Kolkata Knight Riders', 1), ('Rising Pune Supergiant', 0), ('Royal Challengers Bangalore', 0)], '2010': [('Chennai Super Kings', 1), ('Kolkata Knight Riders', 0), ('Rising Pune Supergiant', 0), ('Royal Challengers Bangalore', 1)]}
        total_teams, winning_team = readAndStoreData("matches.csv")

        year_team_value,_=calculate_no_of_win(total_teams, winning_team)
        expected_output=year_team_value
        self.assertEqual(actual_output,expected_output)

        self.assertEqual(readAndStoreData("://matches.csv"),"OS error found")
        self.assertEqual(readAndStoreData(1234),"OS error found")
        self.assertEqual(readAndStoreData("matcgjhes"),"Wrong File path")

        self.assertEqual(calculate_no_of_win("matcgjhes",1234),"Type Error")
        self.assertEqual(calculate_no_of_win("{matcgjhes}",1234),"Type Error")





