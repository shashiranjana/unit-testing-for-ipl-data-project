from checkBatBowlPitch import extract, calculate_avg_score
import unittest

class test_checkBatBowlPich(unittest.TestCase):
    def test(self):

        actual_output ="Key Error"
        expected_output = extract("matches.csv","deliveries.csv")
        self.assertEqual(actual_output,expected_output)

        self.assertEqual(extract("://matches.csv","deliveries.csv"),"OS error found")
        self.assertEqual(extract(1234,"deliveries.csv"),"OS error found")
        self.assertEqual(extract("matcgjhes","deliveries.csv"),"Wrong File path")

        self.assertEqual(calculate_avg_score("matcgjhes"),"Type Error")
        self.assertEqual(calculate_avg_score("{matcgjhes}"),"Type Error")
        self.assertEqual(calculate_avg_score({'a':1}),"Key Error")
