'''
calculating top batting pitch and bowling pitch
'''
import csv
import operator
from matplotlib import pyplot as plt

def extract(matches_file_path, deliveries_file_path):
    '''
    extracting data for bowling and batting pitch
    '''
    try:
        with open(deliveries_file_path, newline='') as deliveries_file, open(
            matches_file_path, newline='') as matches_file:
            deliveries_reader = csv.DictReader(deliveries_file)
            matches_reader = csv.DictReader(matches_file)
            matchId_and_runs = {}
            for delivery in deliveries_reader:
                if delivery['match_id'] in matchId_and_runs:
                    matchId_and_runs[delivery['match_id']] = int(
                        delivery['total_runs']) + int(matchId_and_runs[delivery['match_id']])
                else:
                    matchId_and_runs[delivery['match_id']] = delivery['total_runs']

            matches_in_venue = {'venue':{},'total_matches':{}}
            for match in matches_reader:
                match_id = match['id']
                print(match_id)
                runs_scored = matchId_and_runs[match_id]
                if match['venue'] in matches_in_venue['venue']:
                    matches_in_venue['venue'][match['venue']] = int(runs_scored) + int(
                        matches_in_venue['venue'][match['venue']])
                    matches_in_venue['total_matches'][match['venue']] += 1
                else:
                    matches_in_venue['venue'][match['venue']] = int(runs_scored)
                    matches_in_venue['total_matches'][match['venue']] = 1
            return matches_in_venue
    except FileNotFoundError:
        return "Wrong File path"
    except OSError:
        return "OS error found"
    except KeyError:
        return "Key Error"

def calculate_avg_score(matches_in_venue):
    avg_score_pitch={}
    try:
        for pitch_name in matches_in_venue['total_matches']:
            avg_score=matches_in_venue['venue'][pitch_name]/matches_in_venue['total_matches'][pitch_name]
            avg_score_pitch[pitch_name]=avg_score
        sorted_venue_and_runs = sorted(avg_score_pitch.items(),key=operator.itemgetter(1))
        top_bowling_pitch = sorted_venue_and_runs[:5]
        top_batting_pitch = sorted_venue_and_runs[-5:]
        return top_bowling_pitch, top_batting_pitch
    except ValueError:
        print("1")
        return "value error"
    except TypeError:
        print("2")
        return "Type Error"
    except KeyError:
        print("3")
        return "Key Error"

#extract("matches.csv","deliveries.csv")
#calculate_avg_score('abc.csv')