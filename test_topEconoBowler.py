import unittest
from topEconoBowler import store_match_id,cal_totalRuns_balls,calculate_economy_bowler

class test_noOfMatches(unittest.TestCase):
    def test(self):
        actual_output =['91', '92']
        expected_output = store_match_id(
        "C:\\Users\\shash\\Videos\\Mountblue\\Project\\IPL Data Project\\test\\matches.csv",2008)
        self.assertEqual(actual_output,expected_output)

        actual_output =['2', '3']
        expected_output = store_match_id(
        "C:\\Users\\shash\\Videos\\Mountblue\\Project\\IPL Data Project\\test\\matches.csv",2017)
        self.assertEqual(actual_output,expected_output)

        actual_output =['222', '223']
        expected_output = store_match_id(
        "C:\\Users\\shash\\Videos\\Mountblue\\Project\\IPL Data Project\\test\\matches.csv",2010)
        self.assertEqual(actual_output,expected_output)

        self.assertEqual(store_match_id("://matches.csv",2005),"OS error found")
        self.assertEqual(store_match_id(1234,2008),"OS error found")
        self.assertEqual(store_match_id("matcgjhes","00"),"Wrong File path")

        self.assertEqual(calculate_economy_bowler("matcgjhes"),"Type Error")
        self.assertEqual(calculate_economy_bowler("{matcgjhes}"),"Type Error")




