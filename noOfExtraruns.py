'''
calculating no of extra runs per team
'''
import csv
from matplotlib import pyplot as plt

def extract(matches_file_path, deliveries_file_path):
    '''
    extraction of extractions data
    '''
    try:
        with open(deliveries_file_path, newline='') as deliveries_file, open(
            matches_file_path, newline='') as matches_file:
            deliveries_reader = csv.DictReader(deliveries_file)
            matches_reader = csv.DictReader(matches_file)
            total_match_id = {}
            for match_id in matches_reader:
                if match_id['season'] in total_match_id:
                    total_match_id[match_id['season']] = match_id[
                        'id']+","+total_match_id[match_id['season']]
                else:
                    total_match_id[match_id['season']] = match_id['id']
            total_id_in_2016 = total_match_id['2008'].split(",")
            extra_run_conceded_by_team = {}
            for deliveries_id in deliveries_reader:
                if deliveries_id['match_id'] in total_id_in_2016:
                    if deliveries_id['bowling_team'] in extra_run_conceded_by_team:
                        extra_run_conceded_by_team[deliveries_id['bowling_team']] = int(
                            deliveries_id['extra_runs']) + int(
                                extra_run_conceded_by_team[deliveries_id['bowling_team']])
                    else:
                        extra_run_conceded_by_team[deliveries_id['bowling_team']] = int(
                            deliveries_id['extra_runs'])
            return extra_run_conceded_by_team
    except FileNotFoundError:
        return "Wrong File path"
    except OSError:
        return "OS error found"
    except TypeError:
        return "Type Error"
