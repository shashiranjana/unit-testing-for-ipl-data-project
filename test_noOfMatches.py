from noOfMatches import extract
import unittest

class test_noOfMatches(unittest.TestCase):
    def test(self):
        actual_output ={'2017':2,'2008':2,'2010':2}
        expected_output = extract("matches.csv")
        self.assertEqual(actual_output,expected_output)

        actual_output ="Wrong File path"
        expected_output = extract("matchese.csv")
        self.assertEqual(actual_output,expected_output)

        fileName='matches.csv'
        actual_output=['matches','csv']
        expected_output=fileName.split('.')
        self.assertEqual(expected_output,actual_output)

        self.assertEqual(extract("://matches.csv"),"OS error found")
        self.assertEqual(extract(1234),"OS error found")
        self.assertEqual(extract("matcgjhes"),"Wrong File path")






