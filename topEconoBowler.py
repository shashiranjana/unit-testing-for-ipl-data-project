'''
calculating top economy of bowler
'''
import csv
import operator
from matplotlib import pyplot as plt

def store_match_id(matches_file_path,year):
    '''
    storing match id value of 2015
    '''
    try:
        with open(matches_file_path, newline='') as matches_file:
            matches_reader = csv.DictReader(matches_file)
            season_with_id = {}
            for match in matches_reader:
                season = match['season']
                match_id = match['id']
                if int(season) == year:
                    if season in season_with_id:
                        season_with_id[season] = season_with_id[season]+" " + match_id
                    else:
                        season_with_id[season] = match_id
        key_year=str(year)
        list_of_match_id = season_with_id[key_year].split()
        print(list_of_match_id)
        return list_of_match_id
    except FileNotFoundError:
        return "Wrong File path"
    except OSError:
        return "OS error found"
    except TypeError:
        return "Type Error"


def cal_totalRuns_balls(deliveries_file_path, list_of_match_id):
    '''
    calculate economy of bowler
    '''

    with open(deliveries_file_path, newline='') as deliveries_file:
        deliveries_reader = csv.DictReader(deliveries_file)
        total_runs_and_ball = {'total_runs_conceded':{}, 'bowler_wise_ball':{}}
        for delivery in deliveries_reader:
            is_super_over = int(delivery['is_super_over'])
            current_match_id = delivery['match_id']
            if is_super_over == 0:
                if current_match_id in list_of_match_id:
                    wide_runs = delivery['wide_runs']
                    no_ball_runs = delivery['noball_runs']
                    batsman_runs = delivery['batsman_runs']
                    bowlers_name = delivery['bowler']
                    total_runs = int(wide_runs)+int(no_ball_runs)+int(batsman_runs)
                    if bowlers_name in total_runs_and_ball['total_runs_conceded']:
                        total_runs_and_ball['total_runs_conceded'][bowlers_name] = total_runs + int(
                            total_runs_and_ball['total_runs_conceded'][bowlers_name])
                        if int(wide_runs) == 0 and int(no_ball_runs) == 0:
                            total_runs_and_ball['bowler_wise_ball'][bowlers_name] += 1
                    else:
                        total_runs_and_ball['total_runs_conceded'][bowlers_name] = total_runs
                        if int(wide_runs) == 0 and int(no_ball_runs) == 0:
                            total_runs_and_ball['bowler_wise_ball'][bowlers_name] = 1
                        else:
                            total_runs_and_ball['bowler_wise_ball'][bowlers_name] = 0
        return total_runs_and_ball

def calculate_economy_bowler(total_runs_and_ball):
    '''
    calculate economy of bowler
    '''
    try:
        economy_of_bowler = {}
        for bowler_name in total_runs_and_ball['total_runs_conceded']:
            economy = (total_runs_and_ball['total_runs_conceded'][
                bowler_name]*6)/total_runs_and_ball['bowler_wise_ball'][bowler_name]
            economy_of_bowler[bowler_name] = economy
        economy_of_bowler = sorted(economy_of_bowler.items(), key=operator.itemgetter(1))
        bowler_name = []
        economy_of_bowlers = []
        for value in economy_of_bowler[:10]:
            bowler_name.append(value[0])
            economy_of_bowlers.append(round(value[1],3))
        return bowler_name, economy_of_bowlers
    except ValueError:
        return "value error"
    except TypeError:
        return "Type Error"





