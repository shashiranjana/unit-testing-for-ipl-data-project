'''
calculating no of matches per team
'''
import csv
from matplotlib import pyplot as plt

def extract(filename):
    '''
    calculating no. of matches played by team per season
    '''
    try:
        with open(filename, newline='') as matches_file:
            matches_reader = csv.DictReader(matches_file)

            matches_per_season = {}
            for match in matches_reader:
                season = match['season']
                if (season in matches_per_season):
                    matches_per_season[season] += 1
                else:
                    matches_per_season[season] = 1
            print(matches_per_season)
            return matches_per_season
    except FileNotFoundError:
        return "Wrong File path"
    except OSError:
        return "OS error found"
    except TypeError:
        return "Type Error"


#extract("matches1.csv")